# Kafka WebView

[Kafka WebView](https://www.sourcelab.org/) presents an easy-to-use web based interface for reading data out of kafka topics and providing basic filtering and searching capabilities.

## TL;DR;

```console
$ git clone https://gitlab.com/make-a-bag/public/kafka-webview-chart.git
$ helm install ./kafka-webview-chart
```

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ git clone https://gitlab.com/make-a-bag/public/kafka-webview-chart.git
$ helm install --name my-release ./kafka-webview-chart
```

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Configuration

No application specific values required/provided, see `values.yaml` for common values.

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.
